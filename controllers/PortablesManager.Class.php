<?php


Class PortablesManager {

      private $_db;  // PDO Instance

      public function __construct($db) {

	    $this->setDb($db);

      }

      public function add(Portable $portable) {
	    
	    if ($portable->hasValidMac()) {
		  if ($this->hasUniqueMac($portable)) {
			      $q = $this->_db->prepare('INSERT INTO portables SET mac = :mac, adherent_id = :adherent_id, created_at = :created_at,
		 updated_at = :updated_at, last_seen = :last_seen');
		
			      $q->bindValue(':mac', $portable->to_mac());
			      $q->bindValue(':adherent_id', $portable->getAdherent_id()); 
			      $q->bindValue(':created_at', $portable->getCreated_at());
			      $q->bindValue(':updated_at', $portable->getUpdated_at());
			      $q->bindValue(':last_seen', $portable->getLast_seen());

			      $q->execute() or die(print_r($q->errorInfo(), true));

			      header('Location: adherent.php');
			}
			else
			      echo 'Cette addresse MAC est déjà utilisée.';
	    }
	    else
		  echo 'Veuillez entrer une addresse MAC valide.';
      }

	public function delete ($id) {

		$this->_db->exec('DELETE FROM portables WHERE id = '.$id);
	}


	public function get ($id) {

		$id = (int) $id;

		$q = $this->_db->query('SELECT id, mac, adherent_id, created_at, updated_at, last_seen FROM portables WHERE id= '.$id) ;

		$data = $q->fetch(PDO::FETCH_ASSOC);

		return new Portable($data);
	}


	public function getList($adherent_id) {

		$portables = array();

		$q = $this->_db->prepare('SELECT id, mac FROM portables WHERE adherent_id = :adherent_id ORDER BY created_at');
		$q->bindValue('adherent_id', $adherent_id);

		$q->execute() or die(print_r($q->errorInfo(), true)) ;

		while($mac = $q-> fetch(PDO::FETCH_ASSOC)){
			$macarray[] = $mac ;
		}

		return $macarray;

	}


	public function update(Portable $portable) {

		$q = $this->_db->prepare('INSERT INTO portables SET mac = :mac, adherent_id = :adherent_id, created_at = :created_at,
		 updated_at = :updated_at, last_seen = :last_seen WHERE id=:id');

		$q->bindValue(':mac', $portable->getMac());
		$q->bindValue(':adherent_id', $portable->getAdherent_id());
		$q->bindValue(':created_at', $portable->getCreated_at());
		$q->bindValue(':updated_at', $portable->getUpdated_at());
		$q->bindValue(':last_seen', $portable->getLast_seen());
		$q->bindValue(':id', $portable->getId(), PDO::PARAM_INT);

		$q->execute() or die(print_r($q->errorInfo(), true));
		
	}

	public function hasUniqueMac(Portable $portable) {

	      return !(bool) $this->_db->query('SELECT COUNT(id) FROM portables WHERE mac = "'.$portable->to_mac().'"')->fetchColumn();

	}
	
	public function setDb (PDO $db) {

	      $this->_db = $db;

	}




}

?>