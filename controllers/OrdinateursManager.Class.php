<?php 


Class OrdinateursManager {

	private $_db ;		// PDO Instance .


	public function __construct($db) {

		$this->setDb($db) ;

	}


	public function add (Ordinateur $ordinateur) {
	      if ($ordinateur->hasValidMac()) {
		    if ($this->hasUniqueMac($ordinateur)) {

			$q = $this->_db->prepare('INSERT INTO ordinateurs SET mac = :mac, ip = :ip, adherent_id = :adherent_id, created_at = :created_at,
				updated_at = :updated_at, last_seen = :last_seen, ipv6 = :ipv6 ');


			$q->bindValue(':mac', $ordinateur->to_mac());



			$q->bindValue(':ip', $ordinateur->getIp());
			$q->bindValue(':adherent_id', $ordinateur->getAdherent_id()); 
			$q->bindValue(':created_at', $ordinateur->getCreated_at());
			$q->bindValue(':updated_at', $ordinateur->getUpdated_at());
			$q->bindValue(':last_seen', $ordinateur->getLast_seen());
			$q->bindValue(':ipv6', $ordinateur->getIpv6());


			$q->execute() or die(print_r($q->errorInfo(), true));
			header('Location: adherent.php');
		    }
		    else
			  echo 'Cette adresse MAC est déjà utilisée.';
	      }
	      else
		    echo 'Veuillez entrer une adresse MAC valide';
	}

	public function delete ($id) {

		$this->_db->exec('DELETE FROM ordinateurs WHERE id = '.$id);
	}


	public function get ($id) {

		$id = (int) $id;

		$q = $this->_db->query('SELECT id, mac, ip, adherent_id, created_at, updated_at,
			last_seen, ipv6 FROM ordinateurs WHERE id= '.$id) ;

		$data = $q->fetch(PDO::FETCH_ASSOC);

		return new Ordinateur($data);
	}


	public function getList($adherent_id) {

		$ordinateurs=array();

		$q= $this->_db->prepare('SELECT id, mac, ip, ipv6 FROM ordinateurs WHERE adherent_id=:adherent_id ORDER BY created_at');
		$q->bindValue('adherent_id', $adherent_id);
		$q->execute() or die(print_r($q->errorInfo(), true)) ;

		while($mac = $q-> fetch(PDO::FETCH_ASSOC)){
			$macarray[] = $mac ;
		}

		return $macarray;

	}


	public function update (Ordinateur $ordinateur) {

		$q = $this->_db->prepare('INSERT INTO ordinateurs SET mac = :mac, ip = :ip, adherent_id = :adherent_id, created_at = :created_at,
			updated_at = :updated_at, last_seen = :last_seen, ipv6 = :ipv6  WHERE id=:id');

		$q->bindValue(':mac', $ordinateur->getMac());
		$q->bindValue(':ip', $ordinateur->getIp());
		$q->bindValue(':adherent_id', $ordinateur->getAdherent_id());
		$q->bindValue(':created_at', $ordinateur->getCreated_at());
		$q->bindValue(':updated_at', $ordinateur->getUpdated_at());
		$q->bindValue(':last_seen', $ordinateur->getLast_seen());
		$q->bindValue(':ipv6', $ordinateur->getIpv6());
		$q->bindValue(':id', $ordinateur->getId(), PDO::PARAM_INT);

		$q->execute() or die(print_r($q->errorInfo(), true));

	}

	public function hasUniqueMac(Ordinateur $ordinateur) {

	      return !(bool) $this->_db->query('SELECT COUNT(id) FROM ordinateurs WHERE mac = "'.$ordinateur->to_mac().'"')->fetchColumn();

	}


	public function setDb (PDO $db){

		$this ->_db = $db ;
	}






}

?>
