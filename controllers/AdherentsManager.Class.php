<?php

require_once("ntlmHash.php");

class AdherentsManager {

	private $_db;// PDO Instance

	public function __construct($db) {

		$this->setDb($db);

	}

	public function get($login, $password) {

		$q = $this->_db->prepare('SELECT id, nom, prenom, mail, login, password, chambre_id,
      		created_at, updated_at, date_de_depart, commentaires, mode_association, access_token
      		FROM adherents WHERE login = :login AND password = :password');

		$q->bindValue(':login', $login);
		$q->bindValue(':password', $password);

		$q->execute() or die(print_r($q->errorInfo(), true));

		$data = $q->fetch(PDO::FETCH_ASSOC);

		if ($data) {

			return new Adherent($data);

		} else {

			throw new Exception('Login ou mot de passe incorrects');

		}
	}

	public function update(Adherent $adherent) {

		$q = $this->_db->prepare('UPDATE adherents SET password = :password WHERE id = :id');

		$q->bindValue(':password', $adherent->getPassword());
		$q->bindValue(':id', $adherent->getId());

		$q->execute() or die(print_r($q->errorInfo(), true));

	}

	public function setDb(PDO $db) {

		$this->_db = $db;

	}
}

?>