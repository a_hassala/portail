# Portail MiNET #

## Recode the authentication portail for MiNET network users with new features :##

	- Get rid of the ADH5 strong dependency
	- Better viability and longer code support
	- Make the integration to the soon to come MiNET's new website easier


## **Contribution** ##

Feel free to clone, fork and submit pull requests.