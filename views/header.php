<?php
session_start();


require_once(dirname(__FILE__).'/../controllers/autoload.inc.php');
require_once(dirname(__FILE__).'/../models/autoload.inc.php');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Portail MiNET</title>
    <meta name="description" content="An interactive getting started guide for Brackets.">

    <link rel="stylesheet" href="/current/views/css/portail.css">



</head>

<body>
    <div id="enveloppe">


        <header>
            <!--Premier menu-->
            <div class="superpositionnement">
                <div id="menu">
                    <ul>
                        <li><a href="#" class="contact">Contact</a></li>
                        <li><a href="#" class="portail">Support technique</a></li>
                        <li><a href="#" class="tutos">Tutoriaux</a></li>
                    </ul>
                </div>
            </div>
            <!--Deuxième menu-->
            <div class="superpositionnement">
                <nav>
                    <ul>
      <li class="image"><a href="/index.php"><img src="/current/views/images/logo-black.jpg" alt="logo" title="logo_minet"></a></li>
                        <li class="barre"><a href="#" class="test">Accueil</a></li>
                        <li><a href="#" class="test">Events</a></li>
                        <li><a href="#" class="test">Blog</a></li>
                        <li><a href="#" class="test">Wiki</a></li>
                        <li class="no_border"><form method="post" action="">
                            <input type="text" id="champs_recherche" name="Recherche" placeholder="Recherche" />
                            <input type="submit" id="bouton_recherche" value="" />
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
