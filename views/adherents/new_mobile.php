<?php

require_once('../header.php');

$connexion = new Connexion ;

$db=$connexion->init();

if(isset($_SESSION['adherent'])){
	$adherent=unserialize($_SESSION['adherent']) ;

	$data = array('adherent_id' => $adherent->getId(),
		'created_at' => date('m/d/Y h:i:s a', time()),
		'last_seen' => date('m/d/Y h:i:s a', time()),
		'updated_at' => date('m/d/Y h:i:s a', time())
		);

	$portable = new Portable($data);
	$portable_manager = new PortablesManager($db) ;

	echo '
		<br/><h2> Ajouter une adresse MAC  </h2>
		<form class="add-form" method="post" action="">
			<p>Adresse MAC <input type="text" name="new_portable" value="" placeholder="FF:FF:FF:FF:FF:FF"></p>
			<p class="submit"><input type="submit" name="commit" value="Enregistrer"></p>
		</form>';



	if (isset($_POST['new_portable'])) {

		$portable->setMac(strip_tags($_POST['new_portable']));
		
		$portable_manager->add($portable);

	}


echo"<a href='adherent.php'><span class='button'>>Revenir à l'adhérent</span></a>";

}
else {

	header("Location: logout.php");
}

require_once('../footer.php');

?>
