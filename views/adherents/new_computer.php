<?php
require_once('../header.php');


$connexion = new Connexion ;

$db=$connexion->init();

if(isset($_SESSION['adherent'])){
      
	$adherent = unserialize($_SESSION['adherent']) ;

	$data = array('adherent_id' => $adherent->getId(),
		'created_at' => date('m/d/Y h:i:s a', time()),
		'last_seen' => date('m/d/Y h:i:s a', time()),
		'updated_at' => date('m/d/Y h:i:s a', time())
		);

	$computer = new Ordinateur($data);
	$computer_manager = new OrdinateursManager($db) ;

	echo '<br/><h2> Ajouter une adresse MAC  </h2>
		<form class="add-form" method="post" action="">
			<p>Adresse MAC <input type="text" name="new_computer" value="" placeholder="FF:FF:FF:FF:FF:FF"></p><br/>
			<p class="submit"><input type="submit" name="commit" value="Enregistrer"></p>
		</form>';


	if ( isset($_POST['new_computer'])) {

		$computer->setMac(strip_tags($_POST['new_computer']));
		
				
		$computer_manager->add($computer);

	}
	echo"<a href='adherent.php'><span class='button'>>Revenir à l'adhérent</span></a>";

}
else {

	header("Location: logout.php");
}

require_once('../footer.php');

?>
