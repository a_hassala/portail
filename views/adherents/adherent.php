<?php

require('../header.php');

$connexion = new Connexion ;
$db=$connexion->init();

$manager_adherent   = new AdherentsManager($db);
$manager_ordinateur = new OrdinateursManager($db);
$manager_portable = new PortablesManager($db);


if (!isset($_SESSION['adherent'])) {
	if(isset($_POST['login']) && isset($_POST['password'])) {

		$login = strip_tags($_POST['login']);
		$password = NTLMHash(strip_tags($_POST['password']));

		try {

			$adherent = $manager_adherent->get($login, $password);
		}

		catch (Exception $e) {

			header('Location: ../../index.php');
			exit();
		}
	}
}
else
	$adherent = unserialize($_SESSION['adherent']);

if ($adherent) {

	$_SESSION['adherent'] = serialize($adherent);


	echo '<a href="logout.php">Logout</a>';
	echo '<br/><br/> Votre adhésion expire le <strong>' . $adherent->getDate_de_depart() . '</strong>';
	echo '<h2>Connexion filaire</h2>';
	echo '<table class="center">
	<tr class="firstline">
	<td>Adresse MAC</td><td>Adresse IPv4</td><td>Adresse IPv6</td><td><!--Editer --></td><td><!--Supprimer --></td>
	</tr>';
	$arr =$manager_ordinateur->getList($adherent->getId());
	
	foreach ($arr as $key => $value) {echo'<tr>';
		foreach ($value as $key2 => $value2){
			if($key2 != 'id')
			echo  '<td>',$value2,'</td>' ;   
		}

		echo ' <td><a class="confirmation button" href=delete_computer.php?id='.$value['id'].'>>Supprimer</a></td></tr>';
		//echo ' <td><a href=edit_computer.php> Editer <a/></td> ';
	echo'<br/></tr>';}
	echo'</table >';
	echo '<br/><p align="center"><a class="button" href="new_computer.php">>Ajouter</a></p>';

	echo '<br/><h2> Connexion sans fil </h2>' ;
	echo '<table class="center">
	<tr class="firstline">
	<td>Adresse MAC</td><td><!--Editer --></td><td><!--Supprimer --></td>
	</tr>';
	$arr =$manager_portable->getList($adherent->getId());
 
	foreach ($arr as $key => $value) {echo'<tr>';
		foreach ($value as $key2 => $value2){

		      if ($key2 != 'id')
			    echo  '<td>',$value2,'</td>' ;   
		}

		echo '<td><a class="confirmation button" href=delete_mobile.php?id='.$value['id'].'>>Supprimer</a></td><br/>';
		//echo '<td><a href=edit_mobile.php> Editer <a/></td>';
	}
	echo'</table>';
	echo '<br/><p align="center"><a class="button" href="new_mobile.php">>Ajouter</a></p>' ;
	echo    '<script type="text/javascript">
				var elems = document.getElementsByClassName(\'confirmation\');
				var confirmIt = function (e) {
					if (!confirm(\'Supprimer la Mac?\')) e.preventDefault();
				};
				for (var i = 0, l = elems.length; i < l; i++) {
					elems[i].addEventListener(\'click\', confirmIt, false);
				}
			</script>';
	echo '<br/><br/>';
	echo '<p align="center"><a class="button" href="change_password.php">>Changer mon mot de passe</a></p>';

	require('../footer.php');
}
else {
	header('Location: ../../index.php');
	exit();
}

?>