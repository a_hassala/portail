<?php

session_start();

require('../../controllers/autoload.inc.php');
require('../../models/autoload.inc.php');



if(isset($_GET['id'])) {
	
      $id = (int) $_GET['id'];
      $connexion = new Connexion;
      $db = $connexion->init();
      $portable_manager = new PortablesManager($db);


      $portable_manager->delete($id);


      header("Location: adherent.php");

}

?>
