<?php

/* Sert de base aux classes Ordinateur et Portable */

abstract class Machine {

      protected $id;
      protected $mac;
      protected $adherent_id;
      protected $last_seen;
      protected $created_at;
      protected $updated_at;

      /* Fonction d'hydratation */


      public function hydrate(array $data) {

            foreach ($data as $key => $value) {

                  $methode = 'set' . ucfirst($key);

                  if(method_exists($this, $methode)) {

                        $this->$methode($value);
                  }
            }
      }

      /*Getters*/

      public function getId(){return $this->id; }

      public function getMac(){return $this ->mac ; }

      public function getAdherent_id(){ return $this->adherent_id ;}

      public function getCreated_at(){ return $this->created_at ;}

      public function getUpdated_at(){ return $this->updated_at ;}

      public function getLast_seen(){ return $this->last_seen ;}


      /*Setters*/

      public function setId($id) {

           $this->id = (int) $id ;
      }

      public function setMac($mac) {

           $this->mac = $mac;
      }

      public function setAdherent_id ($adherent_id) {
        $this->adherent_id = $adherent_id;
      }


      public function __construct(array $data) { 

      $this->hydrate($data);
      }	


      public function hasValidMac () {
            $mac = $this->getMac();
            
                  // 01:23:45:67:89:ab
            
            if (preg_match('/^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/', $mac))
                  return true;
            
                  // 01-23-45-67-89-ab
            
            if (preg_match('/^([a-fA-F0-9]{2}\-){5}[a-fA-F0-9]{2}$/', $mac))
                  return true;
            
                  // 0123456789ab
            
            else if (preg_match('/^[a-fA-F0-9]{12}$/', $mac))
                  return true;
            
                  // 0123.4567.89ab
            
            else if (preg_match('/^([a-fA-F0-9]{4}\.){2}[a-fA-F0-9]{4}$/', $mac))
                  return true;
            
            else
                  return false;
      
      }

      public function to_mac() {

            $mac = $this->getMac();
            // remove any dots
            $mac =  str_replace(".", "", $mac);

            // replace colons with dashes
            $mac =  str_replace(":", "-", $mac);

            // counting dashes
            $dash_count = substr_count ($mac , "-");

            // insert enough dashes if none exist
            if ($dash_count == 0) {
    
                  $mac =  substr_replace($mac, "-", 2, 0);
                  $mac =  substr_replace($mac, "-", 5, 0);
                  $mac =  substr_replace($mac, "-", 8, 0);
                  $mac =  substr_replace($mac, "-", 11, 0);
                  $mac =  substr_replace($mac, "-", 14, 0);
            }

            // uppercase
            $mac = strtoupper($mac);

            // DE-AD-BE-EF-10-24
            return $mac;

      }

}


?>
