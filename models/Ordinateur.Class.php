<?php

Class Ordinateur extends Machine {

	/*--------------------------- ATTRIBUTES-------------------------*/

       	private $_ip ;
       	private $_ipv6 ;
	
	/*--------------------------- METHODS ---------------------------*/

	
	
	/*Getters*/
	

	

	public function getIp(){return $this ->_ip ;}

	public function getIpv6(){return $this->_ipv6 ;}

	
	/*Setters*/
	

	public function setIp($ip){

		$this->_ip = ($ip);
	}

	public function setIpv6($ipv6){

		$this->_ipv6 = ($ipv6);
	}

	public static function IsValidIp () {
	      return ((bool) filter_var($this->_ip(), FILTER_VALIDATE_IP, FILTER_FLAG_IPV4));
	}

	public static function IsValidIpv6 () {
	      return ((bool) filter_var($this->_ipv6(), FILTER_VALIDATE_IP, FILTER_FLAG_IPV6));
	}
	
}

?>
