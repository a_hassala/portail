<?php
require('views/header.php');

if (isset($_SESSION['adherent']))
      header('Location: views/adherents/adherent.php');
else
      session_destroy();

?>

<!--Premiere section-->
    <section id="premiere_section">

        <aside>
            <h3><span>Mon</span> compte</h3>
            <form id="login-form" method="post" action="views/adherents/adherent.php">
              <p id="login">Login <input type="text" name="login" value="" placeholder="Username or Email"></p>
              <p>Mot de Passe <input type="password" name="password" value="" placeholder="Password"></p>
            <p class="submit"><input type="submit" name="commit" value="Connexion"></p>
        </form>
        <p></p>
    </aside>

    <article>
        <h4>Bienvenue sur le portail MiNET !</h4>
    </br>
</br>
<p class="texte">Pour pouvoir bénéficier d'une connexion internet dans les logement de la MAISEL. Vous devez être cotisant de l'association MiNET.</br></p>
<p class="texte"></br>A partir de ce portail vous pouvez rajouter les adresses MAC de vos ordinateurs, tablettes et téléphones. Ainsi que changer votre mot de passe.</p>
</article>

</section>

<!--Deuxième section-->
<section id="deuxieme_section">
    
    <aside id="droite">
        <img src="/current/views/images/black.jpg">
    </aside>
    
    <article id="deuxieme_article">
        <h3>Permanences</h3>
    </br>
    <p class="texte">Lundi au jeudi de 18h à 19h30</p>
</br>
<p class="texte"><a href="mailto:problemes@minet.net">Faire part d'un problème</a></p>
</article>

</section>

</div>

<?php
require_once('views/footer.php');
?>
