<?php

function autoloadControllers($classname) {
      
      if (file_exists($file = dirname (__FILE__) . '/' . $classname . '.Class.php'))
	    require $file;
}

spl_autoload_register('autoloadControllers');

?>
