<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Portail MiNET</title>
    <meta name="description" content="An interactive getting started guide for Brackets.">
    <link rel="stylesheet" href="css/portail.css">
</head>

<body>
    <div id="enveloppe">


        <header>
            <!--Premier menu-->
            <div class="superpositionnement">
                <div id="menu">
                    <ul>
                        <li><a href="#" class="contact">Contact</a></li>
                        <li><a href="#" class="portail">Support technique</a></li>
                        <li><a href="#" class="tutos">Tutoriaux</a></li>
                    </ul>
                </div>
            </div>
            <!--Deuxième menu-->
            <div class="superpositionnement">
                <nav>
                    <ul>
                        <li class="image"><a href="#"><img src="Images/logo-black.jpg" alt="logo" title="logo_minet"></a></li>
                        <li class="barre"><a href="#" class="test">Accueil</a></li>
                        <li><a href="#" class="test">Events</a></li>
                        <li><a href="#" class="test">Blog</a></li>
                        <li><a href="#" class="test">Wiki</a></li>
                        <li class="no_border"><form method="post" action="">
                            <input type="text" id="champs_recherche" name="Recherche" placeholder="Recherche" />
                            <input type="submit" id="bouton_recherche" value="" />
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <!--Premiere section-->
    <section id="premiere_section">

        <aside>
            <h3><span>Mon</span> compte</h3>
            <form id="login-form" method="post" action="views/adherents/adherent.php">
              <p id="login">Login <input type="text" name="login" value="" placeholder="Username or Email"></p>
              <p>Mot de Passe <input type="password" name="password" value="" placeholder="Password"></p>
              <p class="remember_me">
                  <label>
                    <input type="checkbox" name="remember_me" id="remember_me">
                    Se souvenir de moi
                </label>
            </p>
            <p class="submit"><input type="submit" name="commit" value="Connexion"></p>
        </form>
        <p></p>
    </aside>

    <article>
        <h4>Bienvenue sur le portail MiNET !</h4>
    </br>
</br>
<p class="texte">Pour pouvoir bénéficier d'une connexion internet dans les logement de la MAISEL. Vous devez être cotisant de l'association MiNET.</br></p>
<p class="texte"></br>A partir de ce portail vous pouvez rajouter les adresses MAC de vos ordinateurs, tablettes et téléphones. Ainsi que changer votre mot de passe.</p>
</article>

</section>

<!--Deuxième section-->
<section id="deuxieme_section">
    
    <aside id="droite">
        <img src="Images/black.jpg">
    </aside>
    
    <article id="deuxieme_article">
        <h3>Permanences</h3>
    </br>
    <p class="texte">Lundi au jeudi de 18h à 19h30</p>
</br>
<p class="texte"><a href="mailto:problemes@minet.net">Faire part d'un problème</a></p>
</article>

</section>

</div>
</body>
</html>