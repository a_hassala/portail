<?php

function autoloadModels($classname) {
      
      if (file_exists($file = dirname (__FILE__) . '/' . $classname . '.Class.php'))
	    require $file;
}

spl_autoload_register('autoloadModels');

?>
