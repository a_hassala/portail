<?php

/* Sert de base aux classes Ordinateur et Portable */

abstract class Machine {

      protected $id;
      protected $mac;
      protected $adherent_id;
      protected $last_seen;
      protected $created_at;
      protected $updated_at;

      /* Fonction d'hydratation */
	

      public function hydrate(array $data) {
	    
	    foreach ($data as $key => $value) {
			
		  $methode = 'set' . ucfirst($key);

		  if(method_exists($this, $methode)) {

			$this->$methode($value);
		  }
	    }
      }

      /*Getters*/

      public function getId(){return $this->id; }

      public function getMac(){return $this ->mac ; }

      public function getAdherent_id(){ return $this->adherent_id ;}

      public function getCreated_at(){ return $this->created_at ;}

      public function getUpdated_at(){ return $this->updated_at ;}

      public function getLast_seen(){ return $this->last_seen ;}


      /*Setters*/

      public function setId($id) {

	    $this->id = (int) $id ;
      }

      public function setMac($mac) {

	    $this->mac = $mac;
      }

      public function setAdherent_id ($adherent_id) {
		$this->adherent_id = $adherent_id;
      }


      public function __construct(array $data) { 

            $this->hydrate($data);
      }	


      public function HasValidMac () {

	    return (preg_match('/([a-fA-F0-9]{2}[:|\-]?){6}/', $this->getMac()) == 1);
      }

      public function to_mac($separator = ':') {

	    return (strtoupper(join($separator, (str_split($this->getMac(), 2)))));

      }

}


?>
