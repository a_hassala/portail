<?php


Class Adherent {


	private $_id;
	private $_nom;
	private $_prenom;
	private $_mail;
	private $_login;
	private $_password;
	private $_chambre_id;
	private $_created_at;
	private $_updated_at;
	private $_date_de_depart;
	private $_commentaires;
	private $_mode_association;
	private $_access_token;







    public function getId()
    {
        return $this->_id;
    }


    public function getPrenom()
    {
        return $this->_prenom;
    }

    public function getNom() {
        return $this->_nom;
    }


    public function getMail()
    {
        return $this->_mail;
    }

 
    public function getLogin()
    {
        return $this->_login;
    }

  
    public function getPassword()
    {
        return $this->_password;
    }


    public function getChambre_id()
    {
        return $this->_chambre_id;
    }


    public function getCreated_at()
    {
        return $this->_created_at;
    }


    public function getUpdated_at()
    {
        return $this->_updated_at;
    }


    public function getDate_de_depart()
    {
        return $this->_date_de_depart;
    }


    public function getCommentaires()
    {
        return $this->_commentaires;
    }


    public function getMode_association()
    {
        return $this->_mode_association;
    }

 
    public function getAccess_token()
    {
        return $this->_access_token;
    }




   



    public function setId($id)
    {
        $this->_id = (int) $id;

        return $this;
    }



    public function setNom($nom)
    {
        $this->_nom = $nom;

        return $this;
    }



    public function setPrenom($prenom)
    {
        $this->_prenom = $prenom;

        return $this;
    }



    public  function setMail($mail)
    {
        $this->_mail = $mail;

        return $this;
    }



    public  function setLogin($login)
    {
        $this->_login = $login;

        return $this;
    }



    public  function setPassword($password)
    {
        $this->_password = $password;

        return $this;
    }





    public  function setChambre_id($chambre_id)
    {
        $this->_chambre_id = $chambre_id;

        return $this;
    }



    public  function setCreated_at($created_at)
    {
        $this->_created_at = $created_at;

        return $this;
    }


    public function setUpdated_at($updated_at) {

    	$this->_updated_at = $updated_at ;

    	return $this;
    }


    public function setDate_de_depart($date_de_depart) {

    	$this->_date_de_depart = $date_de_depart ;

    	return $this;
    }

    public function setCommentaires($commentaires){

    	$this->_commentaires = $commentaires ;

    	return $this;
    }

    public function setMode_association($mode_association) {

    	$this->_mode_association = $mode_association;

    	return $this;
    }

    public function setAccess_token($access_token){

    	$this->_access_token = $access_token ;

    	return $this;
    }



    public function hydrate(array $data) {

		foreach ($data as $key => $value) {
			
			$methode = 'set' . ucfirst($key);

			if(method_exists($this, $methode)) {

				$this->$methode($value);
			}
		}
	}


	public function __construct(array $data) { 

		$this->hydrate($data);
	}	


}


?>