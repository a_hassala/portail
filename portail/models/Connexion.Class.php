<?php


Class Connexion {

	private $_hostname;
	private $_dbname;
	private $_login;
	private $_password;



	public function getHostname(){
		
		return $this->_hostname;
	}


	public function getDbname(){

		return $this->_dbname;
	}


	public function getLogin(){

		return $this->_login;
	}


	public function getPassword(){

		return $this->_password;
	}






	public function setHostname($hostname){

		$this->_hostname = $hostname ;
	}

	public function setDbname($dbname){

		$this->_dbname = $dbname ;
	}

	public function setLogin ($login){

		$this->_login = $login ;
	}

	public function setPassword($password) {

		$this->_password = $password ;
	}







	public function hydrate(array $data) {

		foreach ($data as $key => $value) {
			
			$methode = 'set' . ucfirst($key);

			if(method_exists($this, $methode)) {

				$this->$methode($value);
			}
		}
	}

	public function __construct() {

		$data = array('hostname' => 'localhost',
					  'dbname' => 'adh5',
					  'login' => 'test',
					  'password' => 'test'
					 );
		$this->hydrate($data);
	}


	public function init() {

		try {

		      $dsn = 'mysql:host='.$this->getHostname().';dbname='.$this->getDbname();
		      $db = new PDO($dsn, $this->getLogin(), $this->getPassword()) ;

		      return $db;
		}


		catch (PDOException $e) {

			print"Erreur !: ".$e->getMessage()."<br/>";
			die();
		}

	}



}



?>