<?php
session_start() ;

require('../../controllers/autoload.inc.php');
require('../../models/autoload.inc.php');

require('../../controllers/ntlmHash.php');


$connexion = new Connexion ;

$db=$connexion->init();

if(!isset($_POST['new_password'])) {	
	
	if (!isset($_SESSION['login']) && !isset($_SESSION['password'])) {

		if(isset($_POST['login']) && isset($_POST['password'])) {

			$_SESSION['login'] = strip_tags($_POST['login']);
			$_SESSION['password'] = NTLMHash(strip_tags($_POST['password']));
		}
	}
}

$manager_adherent   = new AdherentsManager($db);
$manager_ordinateur = new OrdinateursManager($db);
$manager_portable = new PortablesManager($db);



try {

	$adherent = $manager_adherent->get($_SESSION['login'], $_SESSION['password']);
}

catch (Exception $e) {

	header('Location: views/adherents/login.php');

	exit();
}


if ($adherent) {

	$_SESSION['adherent'] = serialize($adherent);


	echo '<a href="logout.php">Logout</a>';
	
	echo "<h2> Mac Filaires</h2>";
	
	$arr =$manager_ordinateur->getList($adherent->getId());

	foreach ($arr as $key => $value) {
		foreach ($value as $key2 => $value2){
			
			if($key2 !== 'id'){

			echo '<pre>', $key2 . ' => ' .$value2 ,'</pre>'; ; 
			}
		}

		echo ' <a href=delete_computer.php?id='.$value['id'].'> Supprimer <a/> ';
		//echo ' <a href=edit_computer.php> Editer <a/> ';
	}
	
	echo '<a href="new_computer.php">Ajouter</a>';
	


	echo "<h2> Mac WIFI </h2>" ;

	$arr =$manager_portable->getList($adherent->getId());

	foreach ($arr as $key => $value) {
		foreach ($value as $key2 => $value2){

			echo '<pre>', $key2 . ' => ' .$value2 ,'</pre>'; ; 
		}

		echo ' <a href=delete_mobile.php> Supprimer <a/> ';
		//echo ' <a href=edit_mobile.php> Editer <a/> ';
	}
	

	echo '<a href="new_mobile.php">Ajouter</a>' ;

	echo '<br/> <br/>';
	echo '<a href="change_password.php">Changer mon mot de passe </a>';



}

?>