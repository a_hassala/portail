<?php

session_start();

require('controllers/autoload.inc.php');
require('models/autoload.inc.php');


$connexion = new Connexion ;

$db=$connexion->init();

if(isset($_SESSION['adherent'])){
	$adherent=unserialize($_SESSION['adherent']) ;

	$data = array('adherent_id' => $adherent->getId(),
		'created_at' => date('m/d/Y h:i:s a', time()),
		'last_seen' => date('m/d/Y h:i:s a', time()),
		'updated_at' => date('m/d/Y h:i:s a', time())
		);

	$computer = new Ordinateur($data);
	$computer_manager = new OrdinateursManager($db) ;

	echo '
	<!DOCTYPE>

	<html>
	<head>
		<title> Portail MiNET </title>
		<link href="/assets/application-c0ca111bfd5301de56fa90ca8e657a73.css" media="screen" rel="stylesheet" />
		<meta charset="utf-8">
	</head>


	<body>
		<h2> Ajouter une adresse MAC  </h2>
		<form method="post" action="">
			<p>Adresse MAC <input type="text" name="new_computer" value="" placeholder="FF:FF:FF:FF:FF:FF"></p>
			<p class="submit"><input type="submit" name="commit" value="Enregistrer"></p>
		</form>
	</body>
	</html>' ;




	if ( isset($_POST['new_computer'])) {

		$computer->setMac(strip_tags($_POST['new_computer']));
		
		//var_dump($computer_manager->unique($computer));
		
		if (($computer->getMac() !== null)  && $computer->HasValidMac() && $computer_manager->unique($computer)) {
				
				$computer_manager->add($computer);
				
				//header("Location: adherent.php ");
				
		}

		else if (!$computer_manager->unique($computer) ) {

			echo 'L\'adresse MAC est déjà enregistrée'; 
		}

		else {

			echo 'Veuillez entrer une adresse mac valide !';
		}

	}




}


else {

	header("Location: logout.php");
}