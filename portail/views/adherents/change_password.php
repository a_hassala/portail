<?php
session_start();

require('../../controllers/autoload.inc.php');
require('../../models/autoload.inc.php');

$connexion = new Connexion ;

$db=$connexion->init();


if(isset($_SESSION['adherent'])){
	$adherent=unserialize($_SESSION['adherent']) ;
	$manager_adherent   = new AdherentsManager($db);
echo '
<!DOCTYPE>

<html>
	<head>
		<title> Portail MiNET </title>
		<link href="/assets/application-c0ca111bfd5301de56fa90ca8e657a73.css" media="screen" rel="stylesheet" />
		<meta charset="utf-8">
	</head>


	<body>
		<h2> Changer de mot de passe </h2>
		<form method="post" action="">
			<p>Nouveau Mot de Passe <input type="password" name="new_password" value="" placeholder="Password"></p>
			<p>Confirmation <input type="password" name="confirm_password" value="" placeholder="Password"></p>
			<p class="submit"><input type="submit" name="commit" value="Enregistrer"></p>
		</form>
	</body>
</html>' ;

	if(isset($_POST['new_password']) && isset($_POST['confirm_password'])) {
			
			if($_POST['new_password'] == $_POST['confirm_password'] ) {


				$new_password = NTLMHash(strip_tags($_POST['new_password']));

				echo "new password = " .$new_password ;

				$confirm_password = NTLMHash(strip_tags($_POST['confirm_password']));

				$adherent->setPassword($new_password);

				echo "password de l'adhérent = " . $adherent->getPassword();

				$manager_adherent->update($adherent);

				$_SESSION['login'] = $adherent->getLogin();

				$_SESSION['password'] = $adherent->getPassword();

				header('Location: logout.php');


			}

			else if ($_POST['new_password'] !== $_POST['confirm_password'] ) {

				echo "Veuillez saisir deux fois le même mot de passe";
			}
	}

}

else {

	header("Location: logout.php");
}