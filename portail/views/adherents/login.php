<!DOCTYPE>
<html>
<head>
  <title>Portail MiNET</title>
  <link href="/assets/application-c0ca111bfd5301de56fa90ca8e657a73.css" media="screen" rel="stylesheet" />
  <script src="/assets/application-1e35fffb2b4dee2661b846e86d446ed4.js"></script>
  <meta charset="utf-8">
</head>

<body class="main-index">
  <div id="header">
    <div class="content">
      <a href=""><img width="250" src="/assets/logo.png"></a>
      <a href="/langues?locale=en" alt="en"><img style="float: right" src="/assets/en.png"></a>
      <a href="/langues?locale=es" alt="es"><img style="float: right; margin-right: 10px" src="/assets/es.png"></a>
      <a href="/langues?locale=fr" alt="fr"><img style="float: right; margin-right: 10px" src="/assets/fr.png"></a>
    </div>
  </div>

  <hr />

  <div id="main">
    <div class="content">
      <h2>Mon compte</h2>

      <form method="post" action="adherent.php">
      <p>Login <input type="text" name="login" value="" placeholder="Username or Email"></p>
        <p>Mot de Passe <input type="password" name="password" value="" placeholder="Password"></p>
        <p class="remember_me">
          <label>
            <input type="checkbox" name="remember_me" id="remember_me">
            Remember me on this computer
          </label>
        </p>
        <p class="submit"><input type="submit" name="commit" value="Login"></p>
      </form>
    </div>
  </div>
</body>
</script>
</head>
</html>